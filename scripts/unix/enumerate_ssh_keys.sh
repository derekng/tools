#!/bin/sh

# enumerate through all lines in /etc/passwd
while read line; do

	# extract home directory for each user in /etc/passwd
	USERHOME=$(echo $line | cut -d ":" -f 6)

	# we only care about a user if they have /home/ in their home directory
	case $USERHOME in *"home/"*)

		# print out user's name for organization
		echo "\n"$(echo $USERHOME | cut -d "/" -f 3):
		
		# if a user does not have a .ssh folder then ignore them
		if [ ! -d "$USERHOME/.ssh" ]; then
			continue
		fi

		# loop through all files in user's .ssh directory
		for f in $(sudo ls -a "$USERHOME/.ssh"); do

			# we only care about file if it is a .pub file
			case $f in *".pub")

				# if the ssh key has no comment then print out the entire file
				comment="$(cat "$USERHOME/.ssh/$f" | cut -d " " -f 3)"
				if [ "$comment" = "" ]; then
					cat "$USERHOME/.ssh/$f"

				# otherwise print out just the comment
				else 
					echo $comment
				fi
				;;
			esac
		done
    		;;
	esac

done < /etc/passwd
