# tools

Get help running the CLI:

```bash
❯ python . -h
usage: tools [-h] -p {enumeration,checklist} [-t TASK] [-K]

NUCCDC tools CLI.

options:
  -h, --help            show this help message and exit
  -p {enumeration,checklist}, --playbook {enumeration,checklist}
  -t TASK, --task TASK
  -K, --prompt-sudo-password
```

# Contributing

For initial contributions, please write an Ansible play that conforms to the specification in your assigned issue, and place it into the appropriate playbook directory.

You can test your task individually by running it standalone.

For example, if you are working on a `linux` `checklist` play called `mytask.yml`, you would create a YAML array of Ansible tasks in the `playbooks/checklist/linux/mytask.yml`, and then run:
```bash
❯ python . -p checklist -t linux/mytask
[+] INFO: Running playbook 'checklist' with task 'linux/mytask'
[+] INFO: Found task file at 'playbooks/checklist/linux/mytask.yml'
10.99.1.156 | SUCCESS => {
    "changed": false,
    "include": "playbooks/checklist/linux/mytask.yml",
    "include_args": {}
}
10.99.1.156 | SUCCESS => {
    "msg": "single task!"
}
```

If `sudo` is needed to run a playbook, pass in the `-K` option to have it prompt for the `sudo` password.
